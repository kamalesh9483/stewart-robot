% Simscape(TM) Multibody(TM) version: 7.0

% This is a model data file derived from a Simscape Multibody Import XML file using the smimport function.
% The data in this file sets the block parameter values in an imported Simscape Multibody model.
% For more information on this file, see the smimport function help page in the Simscape Multibody documentation.
% You can modify numerical values, but avoid any other changes to this file.
% Do not add code to this file. Do not edit the physical units shown in comments.

%%%VariableName:smiData


%============= RigidTransform =============%

%Initialize the RigidTransform structure array by filling in null values.
smiData.RigidTransform(91).translation = [0.0 0.0 0.0];
smiData.RigidTransform(91).angle = 0.0;
smiData.RigidTransform(91).axis = [0.0 0.0 0.0];
smiData.RigidTransform(91).ID = '';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(1).translation = [42.752517915708694 -10.000000000000002 -117.46157759823853];  % mm
smiData.RigidTransform(1).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(1).axis = [0.57735026918962584 -0.57735026918962584 0.57735026918962584];
smiData.RigidTransform(1).ID = 'B[Hexapod Base-1:-:Ball Socket 3D-7]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(2).translation = [17.5 -3.5527136788005009e-15 -2.6645352591003757e-15];  % mm
smiData.RigidTransform(2).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(2).axis = [0.57735026918962562 0.57735026918962562 0.57735026918962595];
smiData.RigidTransform(2).ID = 'F[Hexapod Base-1:-:Ball Socket 3D-7]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(3).translation = [0 -10.000000000000002 -125.00000000000003];  % mm
smiData.RigidTransform(3).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(3).axis = [0.57735026918962584 -0.57735026918962584 0.57735026918962584];
smiData.RigidTransform(3).ID = 'B[Hexapod Base-1:-:Ball Socket 3D-8]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(4).translation = [17.500000000000007 1.0658141036401503e-14 -2.8421709430404007e-14];  % mm
smiData.RigidTransform(4).angle = 2.0943951023931957;  % rad
smiData.RigidTransform(4).axis = [0.57735026918962584 0.57735026918962584 0.57735026918962562];
smiData.RigidTransform(4).ID = 'F[Hexapod Base-1:-:Ball Socket 3D-8]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(5).translation = [-123.10096912652379 -10.000000000000002 21.70602220836626];  % mm
smiData.RigidTransform(5).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(5).axis = [0.57735026918962584 -0.57735026918962584 0.57735026918962584];
smiData.RigidTransform(5).ID = 'B[Hexapod Base-1:-:Ball Socket 3D-9]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(6).translation = [17.500000000000011 7.1054273576010019e-15 -1.7763568394002505e-15];  % mm
smiData.RigidTransform(6).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(6).axis = [0.57735026918962584 0.57735026918962584 0.57735026918962584];
smiData.RigidTransform(6).ID = 'F[Hexapod Base-1:-:Ball Socket 3D-9]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(7).translation = [0 0 0];  % mm
smiData.RigidTransform(7).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(7).axis = [-0.57735026918962584 -0.57735026918962584 -0.57735026918962584];
smiData.RigidTransform(7).ID = 'B[Hexapod Base-1:-:Hexapod Top Ring-1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(8).translation = [-81.92605703947703 295.80497327186021 38.830707415151025];  % mm
smiData.RigidTransform(8).angle = 1.7649236489669782;  % rad
smiData.RigidTransform(8).axis = [0.82253994538981567 0.40213681641831744 -0.40213681641831744];
smiData.RigidTransform(8).ID = 'F[Hexapod Base-1:-:Hexapod Top Ring-1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(9).translation = [108.25317547305603 -10.000000000000002 62.499999999997975];  % mm
smiData.RigidTransform(9).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(9).axis = [0.57735026918962584 -0.57735026918962584 0.57735026918962584];
smiData.RigidTransform(9).ID = 'B[Hexapod Base-1:-:Ball Socket 3D-10]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(10).translation = [17.500000000000004 -7.1054273576010019e-15 -1.0658141036401503e-14];  % mm
smiData.RigidTransform(10).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(10).axis = [0.57735026918962584 0.57735026918962584 0.57735026918962584];
smiData.RigidTransform(10).ID = 'F[Hexapod Base-1:-:Ball Socket 3D-10]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(11).translation = [-108.25317547305258 -10.000000000000002 62.500000000000043];  % mm
smiData.RigidTransform(11).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(11).axis = [0.57735026918962584 -0.57735026918962584 0.57735026918962584];
smiData.RigidTransform(11).ID = 'B[Hexapod Base-1:-:Ball Socket 3D-11]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(12).translation = [17.500000000000004 7.1054273576010019e-15 1.3322676295501878e-15];  % mm
smiData.RigidTransform(12).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(12).axis = [0.57735026918962562 0.57735026918962584 0.57735026918962595];
smiData.RigidTransform(12).ID = 'F[Hexapod Base-1:-:Ball Socket 3D-11]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(13).translation = [80.348451210818567 -10.000000000000002 95.755555389870295];  % mm
smiData.RigidTransform(13).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(13).axis = [0.57735026918962584 -0.57735026918962584 0.57735026918962584];
smiData.RigidTransform(13).ID = 'B[Hexapod Base-1:-:Ball Socket 3D-12]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(14).translation = [17.5 -3.5527136788005009e-15 -8.8817841970012523e-16];  % mm
smiData.RigidTransform(14).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(14).axis = [0.57735026918962584 0.57735026918962584 0.57735026918962584];
smiData.RigidTransform(14).ID = 'F[Hexapod Base-1:-:Ball Socket 3D-12]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(15).translation = [0 0 0];  % mm
smiData.RigidTransform(15).angle = 0;  % rad
smiData.RigidTransform(15).axis = [0 0 0];
smiData.RigidTransform(15).ID = 'B[Ball Joint Revolve-1:-:Ball Socket 3D-2]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(16).translation = [1.2621774483536189e-29 -4.4038609109879911e-15 -6.3400109178204549e-14];  % mm
smiData.RigidTransform(16).angle = 0;  % rad
smiData.RigidTransform(16).axis = [0 0 0];
smiData.RigidTransform(16).ID = 'F[Ball Joint Revolve-1:-:Ball Socket 3D-2]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(17).translation = [0 0 0];  % mm
smiData.RigidTransform(17).angle = 0;  % rad
smiData.RigidTransform(17).axis = [0 0 0];
smiData.RigidTransform(17).ID = 'B[Ball Joint Revolve-2:-:Ball Socket 3D-3]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(18).translation = [-7.5730646901217133e-29 3.7868363086447927e-14 -5.1038758917949118e-14];  % mm
smiData.RigidTransform(18).angle = 0;  % rad
smiData.RigidTransform(18).axis = [0 0 0];
smiData.RigidTransform(18).ID = 'F[Ball Joint Revolve-2:-:Ball Socket 3D-3]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(19).translation = [0 0 0];  % mm
smiData.RigidTransform(19).angle = 0;  % rad
smiData.RigidTransform(19).axis = [0 0 0];
smiData.RigidTransform(19).ID = 'B[Ball Joint Revolve-3:-:Ball Socket 3D-4]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(20).translation = [-2.5243548967072378e-29 4.2504720550139116e-14 6.3638448215802477e-14];  % mm
smiData.RigidTransform(20).angle = 0;  % rad
smiData.RigidTransform(20).axis = [0 0 0];
smiData.RigidTransform(20).ID = 'F[Ball Joint Revolve-3:-:Ball Socket 3D-4]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(21).translation = [0 0 0];  % mm
smiData.RigidTransform(21).angle = 0;  % rad
smiData.RigidTransform(21).axis = [0 0 0];
smiData.RigidTransform(21).ID = 'B[Ball Joint Revolve-4:-:Ball Socket 3D-5]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(22).translation = [-5.6843418860808065e-14 -3.0679056313480747e-14 -9.0088421498384029e-15];  % mm
smiData.RigidTransform(22).angle = 0;  % rad
smiData.RigidTransform(22).axis = [0 0 0];
smiData.RigidTransform(22).ID = 'F[Ball Joint Revolve-4:-:Ball Socket 3D-5]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(23).translation = [0 0 0];  % mm
smiData.RigidTransform(23).angle = 0;  % rad
smiData.RigidTransform(23).axis = [0 0 0];
smiData.RigidTransform(23).ID = 'B[Ball Joint Revolve-5:-:Ball Socket 3D-6]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(24).translation = [-6.3108872417680944e-30 2.6071712156793628e-14 -1.1860909182822014e-14];  % mm
smiData.RigidTransform(24).angle = 0;  % rad
smiData.RigidTransform(24).axis = [0 0 0];
smiData.RigidTransform(24).ID = 'F[Ball Joint Revolve-5:-:Ball Socket 3D-6]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(25).translation = [0 0 0];  % mm
smiData.RigidTransform(25).angle = 0;  % rad
smiData.RigidTransform(25).axis = [0 0 0];
smiData.RigidTransform(25).ID = 'B[Ball Joint Revolve-6:-:Ball Socket 3D-1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(26).translation = [5.6843418860808217e-14 -1.8357159938160907e-14 7.9623698612884171e-14];  % mm
smiData.RigidTransform(26).angle = 0;  % rad
smiData.RigidTransform(26).axis = [0 0 0];
smiData.RigidTransform(26).ID = 'F[Ball Joint Revolve-6:-:Ball Socket 3D-1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(27).translation = [0 -199.99999999999991 0];  % mm
smiData.RigidTransform(27).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(27).axis = [0.57735026918962584 -0.57735026918962584 0.57735026918962584];
smiData.RigidTransform(27).ID = 'B[Actuator Tube-1:-:Ball Joint Revolve-7]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(28).translation = [-29.320000000000114 2.4868995751603507e-14 -8.8817841970012523e-15];  % mm
smiData.RigidTransform(28).angle = 2.0943951023931957;  % rad
smiData.RigidTransform(28).axis = [0.5773502691896254 0.57735026918962606 0.57735026918962595];
smiData.RigidTransform(28).ID = 'F[Actuator Tube-1:-:Ball Joint Revolve-7]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(29).translation = [0 0 0];  % mm
smiData.RigidTransform(29).angle = 0;  % rad
smiData.RigidTransform(29).axis = [0 0 0];
smiData.RigidTransform(29).ID = 'B[Ball Joint Revolve-7:-:Ball Socket 3D-12]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(30).translation = [-2.1316282072803018e-14 -1.2562654285184098e-14 4.9674025505151819e-14];  % mm
smiData.RigidTransform(30).angle = 0;  % rad
smiData.RigidTransform(30).axis = [0 0 0];
smiData.RigidTransform(30).ID = 'F[Ball Joint Revolve-7:-:Ball Socket 3D-12]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(31).translation = [0 -199.99999999999994 0];  % mm
smiData.RigidTransform(31).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(31).axis = [0.57735026918962584 -0.57735026918962584 0.57735026918962584];
smiData.RigidTransform(31).ID = 'B[Actuator Tube-6:-:Ball Joint Revolve-8]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(32).translation = [-29.320000000000022 -2.4868995751603507e-14 4.4408920985006262e-15];  % mm
smiData.RigidTransform(32).angle = 2.0943951023931966;  % rad
smiData.RigidTransform(32).axis = [0.57735026918962562 0.57735026918962606 0.57735026918962573];
smiData.RigidTransform(32).ID = 'F[Actuator Tube-6:-:Ball Joint Revolve-8]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(33).translation = [0 0 0];  % mm
smiData.RigidTransform(33).angle = 0;  % rad
smiData.RigidTransform(33).axis = [0 0 0];
smiData.RigidTransform(33).ID = 'B[Ball Joint Revolve-8:-:Ball Socket 3D-10]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(34).translation = [1.421085471520201e-14 -3.0694635979338639e-14 6.5639146149587781e-14];  % mm
smiData.RigidTransform(34).angle = 0;  % rad
smiData.RigidTransform(34).axis = [0 0 0];
smiData.RigidTransform(34).ID = 'F[Ball Joint Revolve-8:-:Ball Socket 3D-10]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(35).translation = [0 -200 0];  % mm
smiData.RigidTransform(35).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(35).axis = [0.57735026918962584 -0.57735026918962584 0.57735026918962584];
smiData.RigidTransform(35).ID = 'B[Actuator Tube-2:-:Ball Joint Revolve-9]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(36).translation = [-29.320000000000036 5.3290705182007514e-14 5.6843418860808015e-14];  % mm
smiData.RigidTransform(36).angle = 2.0943951023931962;  % rad
smiData.RigidTransform(36).axis = [0.57735026918962506 0.57735026918962606 0.57735026918962606];
smiData.RigidTransform(36).ID = 'F[Actuator Tube-2:-:Ball Joint Revolve-9]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(37).translation = [0 0 0];  % mm
smiData.RigidTransform(37).angle = 0;  % rad
smiData.RigidTransform(37).axis = [0 0 0];
smiData.RigidTransform(37).ID = 'B[Ball Joint Revolve-9:-:Ball Socket 3D-11]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(38).translation = [-1.2621774483536189e-29 9.6395278902829183e-14 -7.3825353382263261e-14];  % mm
smiData.RigidTransform(38).angle = 0;  % rad
smiData.RigidTransform(38).axis = [0 0 0];
smiData.RigidTransform(38).ID = 'F[Ball Joint Revolve-9:-:Ball Socket 3D-11]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(39).translation = [0 -200 0];  % mm
smiData.RigidTransform(39).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(39).axis = [0.57735026918962584 -0.57735026918962584 0.57735026918962584];
smiData.RigidTransform(39).ID = 'B[Actuator Tube-3:-:Ball Joint Revolve-10]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(40).translation = [-29.32000000000005 1.5631940186722204e-13 4.4853010194856324e-14];  % mm
smiData.RigidTransform(40).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(40).axis = [0.57735026918962573 0.57735026918962584 0.57735026918962573];
smiData.RigidTransform(40).ID = 'F[Actuator Tube-3:-:Ball Joint Revolve-10]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(41).translation = [0 0 0];  % mm
smiData.RigidTransform(41).angle = 0;  % rad
smiData.RigidTransform(41).axis = [0 0 0];
smiData.RigidTransform(41).ID = 'B[Ball Joint Revolve-10:-:Ball Socket 3D-9]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(42).translation = [-7.1054273576010019e-15 -7.9690001494990054e-14 -6.4426858002160829e-14];  % mm
smiData.RigidTransform(42).angle = 0;  % rad
smiData.RigidTransform(42).axis = [0 0 0];
smiData.RigidTransform(42).ID = 'F[Ball Joint Revolve-10:-:Ball Socket 3D-9]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(43).translation = [0 -200 0];  % mm
smiData.RigidTransform(43).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(43).axis = [0.57735026918962584 -0.57735026918962584 0.57735026918962584];
smiData.RigidTransform(43).ID = 'B[Actuator Tube-4:-:Ball Joint Revolve-11]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(44).translation = [-29.320000000000057 3.1974423109204508e-14 0];  % mm
smiData.RigidTransform(44).angle = 2.0943951023931962;  % rad
smiData.RigidTransform(44).axis = [0.57735026918962529 0.57735026918962595 0.57735026918962606];
smiData.RigidTransform(44).ID = 'F[Actuator Tube-4:-:Ball Joint Revolve-11]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(45).translation = [0 0 0];  % mm
smiData.RigidTransform(45).angle = 0;  % rad
smiData.RigidTransform(45).axis = [0 0 0];
smiData.RigidTransform(45).ID = 'B[Ball Joint Revolve-11:-:Ball Socket 3D-8]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(46).translation = [-7.1054273576009703e-15 6.4866669700668773e-14 -4.748329914311843e-14];  % mm
smiData.RigidTransform(46).angle = 0;  % rad
smiData.RigidTransform(46).axis = [0 0 0];
smiData.RigidTransform(46).ID = 'F[Ball Joint Revolve-11:-:Ball Socket 3D-8]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(47).translation = [0 -199.99999999999994 0];  % mm
smiData.RigidTransform(47).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(47).axis = [0.57735026918962584 -0.57735026918962584 0.57735026918962584];
smiData.RigidTransform(47).ID = 'B[Actuator Tube-5:-:Ball Joint Revolve-12]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(48).translation = [-29.320000000000022 7.1054273576010019e-15 2.1316282072803006e-14];  % mm
smiData.RigidTransform(48).angle = 2.0943951023931939;  % rad
smiData.RigidTransform(48).axis = [0.57735026918962606 0.57735026918962529 0.57735026918962606];
smiData.RigidTransform(48).ID = 'F[Actuator Tube-5:-:Ball Joint Revolve-12]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(49).translation = [0 0 0];  % mm
smiData.RigidTransform(49).angle = 0;  % rad
smiData.RigidTransform(49).axis = [0 0 0];
smiData.RigidTransform(49).ID = 'B[Ball Joint Revolve-12:-:Ball Socket 3D-7]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(50).translation = [-2.1316282072803006e-14 4.3149365980246625e-14 -5.6897531641112116e-14];  % mm
smiData.RigidTransform(50).angle = 0;  % rad
smiData.RigidTransform(50).axis = [0 0 0];
smiData.RigidTransform(50).ID = 'F[Ball Joint Revolve-12:-:Ball Socket 3D-7]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(51).translation = [-131.77921575852832 491.81500338872087 53.620247126134032];  % mm
smiData.RigidTransform(51).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(51).axis = [-0.57735026918962584 -0.57735026918962584 -0.57735026918962584];
smiData.RigidTransform(51).ID = 'B[:-:]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(52).translation = [-131.77921575852832 491.81500338872087 53.620247126134032];  % mm
smiData.RigidTransform(52).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(52).axis = [-0.57735026918962584 -0.57735026918962584 -0.57735026918962584];
smiData.RigidTransform(52).ID = 'F[:-:]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(53).translation = [0 -20.000000000000018 0];  % mm
smiData.RigidTransform(53).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(53).axis = [-0.57735026918962584 -0.57735026918962584 -0.57735026918962584];
smiData.RigidTransform(53).ID = 'B[Hexapod Top Ring-1:-:]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(54).translation = [-108.66326915408929 347.03130677289363 138.61846290620664];  % mm
smiData.RigidTransform(54).angle = 1.7649236489669782;  % rad
smiData.RigidTransform(54).axis = [0.82253994538981567 0.40213681641831744 -0.40213681641831744];
smiData.RigidTransform(54).ID = 'F[Hexapod Top Ring-1:-:]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(55).translation = [108.25317547305768 -10.000000000000009 62.499999999999055];  % mm
smiData.RigidTransform(55).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(55).axis = [-0.57735026918962584 -0.57735026918962584 -0.57735026918962584];
smiData.RigidTransform(55).ID = 'B[Hexapod Top Ring-1:-:Ball Socket 3D-1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(56).translation = [17.500000000000064 3.1455726912099635e-11 -9.4004803941061255e-11];  % mm
smiData.RigidTransform(56).angle = 2.0943951023931944;  % rad
smiData.RigidTransform(56).axis = [-0.57735026918962551 -0.5773502691896254 0.57735026918962629];
smiData.RigidTransform(56).ID = 'F[Hexapod Top Ring-1:-:Ball Socket 3D-1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(57).translation = [42.75251791570868 -10.000000000000009 -117.46157759823858];  % mm
smiData.RigidTransform(57).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(57).axis = [-0.57735026918962584 -0.57735026918962584 -0.57735026918962584];
smiData.RigidTransform(57).ID = 'B[Hexapod Top Ring-1:-:Ball Socket 3D-2]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(58).translation = [17.500000000000064 1.8633272702572867e-10 -2.3389734593592948e-11];  % mm
smiData.RigidTransform(58).angle = 2.0943951023931966;  % rad
smiData.RigidTransform(58).axis = [-0.57735026918962518 -0.57735026918962606 0.57735026918962595];
smiData.RigidTransform(58).ID = 'F[Hexapod Top Ring-1:-:Ball Socket 3D-2]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(59).translation = [0 -10.000000000000009 -125.00000000000009];  % mm
smiData.RigidTransform(59).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(59).axis = [-0.57735026918962584 -0.57735026918962584 -0.57735026918962584];
smiData.RigidTransform(59).ID = 'B[Hexapod Top Ring-1:-:Ball Socket 3D-3]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(60).translation = [17.500000000000064 -8.9592333551991032e-11 -1.6838086480674974e-10];  % mm
smiData.RigidTransform(60).angle = 2.0943951023931957;  % rad
smiData.RigidTransform(60).axis = [-0.57735026918962551 -0.57735026918962595 0.57735026918962573];
smiData.RigidTransform(60).ID = 'F[Hexapod Top Ring-1:-:Ball Socket 3D-3]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(61).translation = [-123.10096912652386 -10.000000000000009 21.706022208368118];  % mm
smiData.RigidTransform(61).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(61).axis = [-0.57735026918962584 -0.57735026918962584 -0.57735026918962584];
smiData.RigidTransform(61).ID = 'B[Hexapod Top Ring-1:-:Ball Socket 3D-4]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(62).translation = [17.50000000000006 -8.4021678503631847e-11 -9.2970964260530309e-11];  % mm
smiData.RigidTransform(62).angle = 2.0943951023931957;  % rad
smiData.RigidTransform(62).axis = [-0.5773502691896254 -0.57735026918962595 0.57735026918962595];
smiData.RigidTransform(62).ID = 'F[Hexapod Top Ring-1:-:Ball Socket 3D-4]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(63).translation = [-108.25317547305272 -10.000000000000009 62.500000000001918];  % mm
smiData.RigidTransform(63).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(63).axis = [-0.57735026918962584 -0.57735026918962584 -0.57735026918962584];
smiData.RigidTransform(63).ID = 'B[Hexapod Top Ring-1:-:Ball Socket 3D-5]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(64).translation = [17.500000000000064 -9.0537355390551966e-11 -4.1602277178753866e-11];  % mm
smiData.RigidTransform(64).angle = 2.0943951023931944;  % rad
smiData.RigidTransform(64).axis = [-0.57735026918962573 -0.5773502691896254 0.57735026918962618];
smiData.RigidTransform(64).ID = 'F[Hexapod Top Ring-1:-:Ball Socket 3D-5]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(65).translation = [80.348451210820173 -10.000000000000009 95.755555389871404];  % mm
smiData.RigidTransform(65).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(65).axis = [-0.57735026918962584 -0.57735026918962584 -0.57735026918962584];
smiData.RigidTransform(65).ID = 'B[Hexapod Top Ring-1:-:Ball Socket 3D-6]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(66).translation = [17.50000000000006 4.8601123125990853e-11 -5.2753357238088938e-11];  % mm
smiData.RigidTransform(66).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(66).axis = [-0.57735026918962584 -0.57735026918962584 0.57735026918962584];
smiData.RigidTransform(66).ID = 'F[Hexapod Top Ring-1:-:Ball Socket 3D-6]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(67).translation = [0 0 0];  % mm
smiData.RigidTransform(67).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(67).axis = [-0.57735026918962584 -0.57735026918962584 -0.57735026918962584];
smiData.RigidTransform(67).ID = 'B[Actuator Cylinder-1:-:Ball Joint Revolve-1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(68).translation = [-29.320000000000014 7.0699002208129968e-13 1.7763568394002505e-13];  % mm
smiData.RigidTransform(68).angle = 2.0943951023931962;  % rad
smiData.RigidTransform(68).axis = [0.57735026918962584 0.57735026918962595 0.5773502691896254];
smiData.RigidTransform(68).ID = 'F[Actuator Cylinder-1:-:Ball Joint Revolve-1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(69).translation = [0 -180.00000000000006 0];  % mm
smiData.RigidTransform(69).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(69).axis = [-0.57735026918962584 -0.57735026918962584 -0.57735026918962584];
smiData.RigidTransform(69).ID = 'B[Actuator Cylinder-1:-:Actuator Tube-1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(70).translation = [-2.7711166694643907e-13 -129.16202904287297 1.5631940186722204e-13];  % mm
smiData.RigidTransform(70).angle = 2.0943951023931966;  % rad
smiData.RigidTransform(70).axis = [-0.57735026918962629 -0.5773502691896254 -0.57735026918962573];
smiData.RigidTransform(70).ID = 'F[Actuator Cylinder-1:-:Actuator Tube-1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(71).translation = [0 0 0];  % mm
smiData.RigidTransform(71).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(71).axis = [-0.57735026918962584 -0.57735026918962584 -0.57735026918962584];
smiData.RigidTransform(71).ID = 'B[Actuator Cylinder-2:-:Ball Joint Revolve-2]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(72).translation = [-29.319999999999951 1.4921397450962104e-13 -3.9968028886505635e-13];  % mm
smiData.RigidTransform(72).angle = 2.0943951023931966;  % rad
smiData.RigidTransform(72).axis = [0.5773502691896254 0.57735026918962618 0.57735026918962573];
smiData.RigidTransform(72).ID = 'F[Actuator Cylinder-2:-:Ball Joint Revolve-2]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(73).translation = [0 -180.00000000000011 0];  % mm
smiData.RigidTransform(73).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(73).axis = [-0.57735026918962584 -0.57735026918962584 -0.57735026918962584];
smiData.RigidTransform(73).ID = 'B[Actuator Cylinder-2:-:Actuator Tube-2]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(74).translation = [-5.6843418860808015e-14 -125.9251021261928 9.9475983006414026e-14];  % mm
smiData.RigidTransform(74).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(74).axis = [-0.57735026918962584 -0.57735026918962584 -0.57735026918962584];
smiData.RigidTransform(74).ID = 'F[Actuator Cylinder-2:-:Actuator Tube-2]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(75).translation = [0 0 0];  % mm
smiData.RigidTransform(75).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(75).axis = [-0.57735026918962584 -0.57735026918962584 -0.57735026918962584];
smiData.RigidTransform(75).ID = 'B[Actuator Cylinder-3:-:Ball Joint Revolve-3]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(76).translation = [-29.319999999999979 1.2789769243681803e-13 -8.5353946133182035e-13];  % mm
smiData.RigidTransform(76).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(76).axis = [0.57735026918962584 0.57735026918962562 0.57735026918962595];
smiData.RigidTransform(76).ID = 'F[Actuator Cylinder-3:-:Ball Joint Revolve-3]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(77).translation = [0 -180 0];  % mm
smiData.RigidTransform(77).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(77).axis = [-0.57735026918962584 -0.57735026918962584 -0.57735026918962584];
smiData.RigidTransform(77).ID = 'B[Actuator Cylinder-3:-:Actuator Tube-3]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(78).translation = [-1.2789769243681803e-13 -144.72384372670675 -1.4210854715202004e-13];  % mm
smiData.RigidTransform(78).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(78).axis = [-0.57735026918962573 -0.57735026918962584 -0.57735026918962573];
smiData.RigidTransform(78).ID = 'F[Actuator Cylinder-3:-:Actuator Tube-3]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(79).translation = [0 0 0];  % mm
smiData.RigidTransform(79).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(79).axis = [-0.57735026918962584 -0.57735026918962584 -0.57735026918962584];
smiData.RigidTransform(79).ID = 'B[Actuator Cylinder-4:-:Ball Joint Revolve-4]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(80).translation = [-29.319999999999979 3.4816594052244909e-13 4.8316906031686813e-13];  % mm
smiData.RigidTransform(80).angle = 2.0943951023931957;  % rad
smiData.RigidTransform(80).axis = [0.57735026918962506 0.57735026918962584 0.5773502691896264];
smiData.RigidTransform(80).ID = 'F[Actuator Cylinder-4:-:Ball Joint Revolve-4]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(81).translation = [0 -179.99999999999994 0];  % mm
smiData.RigidTransform(81).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(81).axis = [-0.57735026918962584 -0.57735026918962584 -0.57735026918962584];
smiData.RigidTransform(81).ID = 'B[Actuator Cylinder-4:-:Actuator Tube-4]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(82).translation = [-5.3290705182007514e-14 -143.65247584280553 -1.9184653865522705e-13];  % mm
smiData.RigidTransform(82).angle = 2.0943951023931948;  % rad
smiData.RigidTransform(82).axis = [-0.57735026918962551 -0.57735026918962551 -0.57735026918962629];
smiData.RigidTransform(82).ID = 'F[Actuator Cylinder-4:-:Actuator Tube-4]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(83).translation = [0 0 0];  % mm
smiData.RigidTransform(83).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(83).axis = [-0.57735026918962584 -0.57735026918962584 -0.57735026918962584];
smiData.RigidTransform(83).ID = 'B[Actuator Cylinder-5:-:Ball Joint Revolve-5]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(84).translation = [-29.32 -1.0231815394945443e-12 -2.1849189124623081e-13];  % mm
smiData.RigidTransform(84).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(84).axis = [0.57735026918962684 0.57735026918962562 0.57735026918962473];
smiData.RigidTransform(84).ID = 'F[Actuator Cylinder-5:-:Ball Joint Revolve-5]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(85).translation = [0 -180 0];  % mm
smiData.RigidTransform(85).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(85).axis = [-0.57735026918962584 -0.57735026918962584 -0.57735026918962584];
smiData.RigidTransform(85).ID = 'B[Actuator Cylinder-5:-:Actuator Tube-5]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(86).translation = [2.4868995751603507e-14 -87.968049450570888 -1.2079226507921703e-13];  % mm
smiData.RigidTransform(86).angle = 2.0943951023931957;  % rad
smiData.RigidTransform(86).axis = [-0.57735026918962573 -0.57735026918962573 -0.57735026918962584];
smiData.RigidTransform(86).ID = 'F[Actuator Cylinder-5:-:Actuator Tube-5]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(87).translation = [0 0 0];  % mm
smiData.RigidTransform(87).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(87).axis = [-0.57735026918962584 -0.57735026918962584 -0.57735026918962584];
smiData.RigidTransform(87).ID = 'B[Actuator Cylinder-6:-:Ball Joint Revolve-6]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(88).translation = [-29.319999999999965 -3.907985046680551e-14 7.5139894306630595e-13];  % mm
smiData.RigidTransform(88).angle = 2.0943951023931948;  % rad
smiData.RigidTransform(88).axis = [0.57735026918962562 0.57735026918962551 0.57735026918962618];
smiData.RigidTransform(88).ID = 'F[Actuator Cylinder-6:-:Ball Joint Revolve-6]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(89).translation = [0 -180 0];  % mm
smiData.RigidTransform(89).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(89).axis = [-0.57735026918962584 -0.57735026918962584 -0.57735026918962584];
smiData.RigidTransform(89).ID = 'B[Actuator Cylinder-6:-:Actuator Tube-6]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(90).translation = [3.1974423109204508e-14 -150.026360662157 4.2632564145606011e-14];  % mm
smiData.RigidTransform(90).angle = 2.0943951023931962;  % rad
smiData.RigidTransform(90).axis = [-0.57735026918962595 -0.57735026918962595 -0.5773502691896254];
smiData.RigidTransform(90).ID = 'F[Actuator Cylinder-6:-:Actuator Tube-6]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(91).translation = [-149.46754773427529 31.226333501033427 57.657277700286613];  % mm
smiData.RigidTransform(91).angle = 0;  % rad
smiData.RigidTransform(91).axis = [0 0 0];
smiData.RigidTransform(91).ID = 'RootGround[Hexapod Base-1]';


%============= Solid =============%
%Center of Mass (CoM) %Moments of Inertia (MoI) %Product of Inertia (PoI)

%Initialize the Solid structure array by filling in null values.
smiData.Solid(6).mass = 0.0;
smiData.Solid(6).CoM = [0.0 0.0 0.0];
smiData.Solid(6).MoI = [0.0 0.0 0.0];
smiData.Solid(6).PoI = [0.0 0.0 0.0];
smiData.Solid(6).color = [0.0 0.0 0.0];
smiData.Solid(6).opacity = 0.0;
smiData.Solid(6).ID = '';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(1).mass = 0.11525564966781858;  % kg
smiData.Solid(1).CoM = [-10.431431311361052 0 0];  % mm
smiData.Solid(1).MoI = [6.4933175571844659 21.09174033360566 21.09174033360566];  % kg*mm^2
smiData.Solid(1).PoI = [0 0 0];  % kg*mm^2
smiData.Solid(1).color = [0.6470588235294118 0.51764705882352935 0];
smiData.Solid(1).opacity = 1;
smiData.Solid(1).ID = 'Ball Joint Revolve*:*Default';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(2).mass = 0.038285552943723204;  % kg
smiData.Solid(2).CoM = [7.3228529614734814 0 0];  % mm
smiData.Solid(2).MoI = [7.1649453778924022 5.3716624432362874 5.3716624432362874];  % kg*mm^2
smiData.Solid(2).PoI = [0 0 0];  % kg*mm^2
smiData.Solid(2).color = [0.48862745098039212 0.48862745098039212 0.48862745098039212];
smiData.Solid(2).opacity = 1;
smiData.Solid(2).ID = 'Ball Socket 3D*:*Default';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(3).mass = 0.63197201459586949;  % kg
smiData.Solid(3).CoM = [0 -90.416435081517776 0];  % mm
smiData.Solid(3).MoI = [1715.845176754199 45.717645073918852 1715.845176754199];  % kg*mm^2
smiData.Solid(3).PoI = [0 0 0];  % kg*mm^2
smiData.Solid(3).color = [0.89803921568627454 0.89803921568627454 0.89803921568627454];
smiData.Solid(3).opacity = 1;
smiData.Solid(3).ID = 'Actuator Cylinder*:*Default';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(4).mass = 0.055134951070500822;  % kg
smiData.Solid(4).CoM = [0 -107.30769230769235 0];  % mm
smiData.Solid(4).MoI = [209.8705659520929 9.7143542636140143 209.8705659520929];  % kg*mm^2
smiData.Solid(4).PoI = [0 0 0];  % kg*mm^2
smiData.Solid(4).color = [1 1 1];
smiData.Solid(4).opacity = 1;
smiData.Solid(4).ID = 'Actuator Tube*:*Default';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(5).mass = 3.6611728085853392;  % kg
smiData.Solid(5).CoM = [0 -10.212858384013952 -5.5219962132509644e-13];  % mm
smiData.Solid(5).MoI = [20363.088348419311 40482.430275498991 20363.088348419016];  % kg*mm^2
smiData.Solid(5).PoI = [0 0 0];  % kg*mm^2
smiData.Solid(5).color = [0.89803921568627454 0.92156862745098034 0.92941176470588238];
smiData.Solid(5).opacity = 1;
smiData.Solid(5).ID = 'Hexapod Top Ring*:*Default';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(6).mass = 3.6611728085853281;  % kg
smiData.Solid(6).CoM = [0 -10.212858384013968 0];  % mm
smiData.Solid(6).MoI = [20363.088348419093 40482.430275498846 20363.088348419085];  % kg*mm^2
smiData.Solid(6).PoI = [0 0 0];  % kg*mm^2
smiData.Solid(6).color = [0.8666666666666667 0.8666666666666667 0.8901960784313725];
smiData.Solid(6).opacity = 1;
smiData.Solid(6).ID = 'Hexapod Base*:*Default';


%============= Joint =============%
%X Revolute Primitive (Rx) %Y Revolute Primitive (Ry) %Z Revolute Primitive (Rz)
%X Prismatic Primitive (Px) %Y Prismatic Primitive (Py) %Z Prismatic Primitive (Pz) %Spherical Primitive (S)
%Constant Velocity Primitive (CV) %Lead Screw Primitive (LS)
%Position Target (Pos)

%Initialize the RevoluteJoint structure array by filling in null values.
smiData.RevoluteJoint(30).Rz.Pos = 0.0;
smiData.RevoluteJoint(30).ID = '';

smiData.RevoluteJoint(1).Rz.Pos = 137.11382788878555;  % deg
smiData.RevoluteJoint(1).ID = '[Hexapod Base-1:-:Ball Socket 3D-7]';

smiData.RevoluteJoint(2).Rz.Pos = 171.20464355667318;  % deg
smiData.RevoluteJoint(2).ID = '[Hexapod Base-1:-:Ball Socket 3D-8]';

smiData.RevoluteJoint(3).Rz.Pos = 174.73561881214391;  % deg
smiData.RevoluteJoint(3).ID = '[Hexapod Base-1:-:Ball Socket 3D-9]';

smiData.RevoluteJoint(4).Rz.Pos = 143.62800824859303;  % deg
smiData.RevoluteJoint(4).ID = '[Hexapod Base-1:-:Ball Socket 3D-10]';

smiData.RevoluteJoint(5).Rz.Pos = 106.89103840166092;  % deg
smiData.RevoluteJoint(5).ID = '[Hexapod Base-1:-:Ball Socket 3D-11]';

smiData.RevoluteJoint(6).Rz.Pos = 132.11731117935273;  % deg
smiData.RevoluteJoint(6).ID = '[Hexapod Base-1:-:Ball Socket 3D-12]';

smiData.RevoluteJoint(7).Rz.Pos = -70.226551798258171;  % deg
smiData.RevoluteJoint(7).ID = '[Actuator Tube-1:-:Ball Joint Revolve-7]';

smiData.RevoluteJoint(8).Rz.Pos = -89.058990818859471;  % deg
smiData.RevoluteJoint(8).ID = '[Actuator Tube-6:-:Ball Joint Revolve-8]';

smiData.RevoluteJoint(9).Rz.Pos = 77.61946061356204;  % deg
smiData.RevoluteJoint(9).ID = '[Actuator Tube-2:-:Ball Joint Revolve-9]';

smiData.RevoluteJoint(10).Rz.Pos = -89.999999999999872;  % deg
smiData.RevoluteJoint(10).ID = '[Actuator Tube-3:-:Ball Joint Revolve-10]';

smiData.RevoluteJoint(11).Rz.Pos = 18.838831974969043;  % deg
smiData.RevoluteJoint(11).ID = '[Actuator Tube-4:-:Ball Joint Revolve-11]';

smiData.RevoluteJoint(12).Rz.Pos = -59.110668602787214;  % deg
smiData.RevoluteJoint(12).ID = '[Actuator Tube-5:-:Ball Joint Revolve-12]';

smiData.RevoluteJoint(13).Rz.Pos = -24.909681816223266;  % deg
smiData.RevoluteJoint(13).ID = '[Hexapod Top Ring-1:-:Ball Socket 3D-1]';

smiData.RevoluteJoint(14).Rz.Pos = 74.699273005584843;  % deg
smiData.RevoluteJoint(14).ID = '[Hexapod Top Ring-1:-:Ball Socket 3D-2]';

smiData.RevoluteJoint(15).Rz.Pos = -27.883685797128663;  % deg
smiData.RevoluteJoint(15).ID = '[Hexapod Top Ring-1:-:Ball Socket 3D-3]';

smiData.RevoluteJoint(16).Rz.Pos = -3.4330600233284465;  % deg
smiData.RevoluteJoint(16).ID = '[Hexapod Top Ring-1:-:Ball Socket 3D-4]';

smiData.RevoluteJoint(17).Rz.Pos = -21.527546460351797;  % deg
smiData.RevoluteJoint(17).ID = '[Hexapod Top Ring-1:-:Ball Socket 3D-5]';

smiData.RevoluteJoint(18).Rz.Pos = -2.0995999153047586;  % deg
smiData.RevoluteJoint(18).ID = '[Hexapod Top Ring-1:-:Ball Socket 3D-6]';

smiData.RevoluteJoint(19).Rz.Pos = -150.02918269689548;  % deg
smiData.RevoluteJoint(19).ID = '[Actuator Cylinder-1:-:Ball Joint Revolve-1]';

smiData.RevoluteJoint(20).Rz.Pos = -40.255734495153511;  % deg
smiData.RevoluteJoint(20).ID = '[Actuator Cylinder-1:-:Actuator Tube-1]';

smiData.RevoluteJoint(21).Rz.Pos = 35.871476408489421;  % deg
smiData.RevoluteJoint(21).ID = '[Actuator Cylinder-2:-:Ball Joint Revolve-2]';

smiData.RevoluteJoint(22).Rz.Pos = -66.509062977948389;  % deg
smiData.RevoluteJoint(22).ID = '[Actuator Cylinder-2:-:Actuator Tube-2]';

smiData.RevoluteJoint(23).Rz.Pos = -5.9907708333527774;  % deg
smiData.RevoluteJoint(23).ID = '[Actuator Cylinder-3:-:Ball Joint Revolve-3]';

smiData.RevoluteJoint(24).Rz.Pos = 84.009229166647543;  % deg
smiData.RevoluteJoint(24).ID = '[Actuator Cylinder-3:-:Actuator Tube-3]';

smiData.RevoluteJoint(25).Rz.Pos = 147.80154945993209;  % deg
smiData.RevoluteJoint(25).ID = '[Actuator Cylinder-4:-:Ball Joint Revolve-4]';

smiData.RevoluteJoint(26).Rz.Pos = -13.359618565098893;  % deg
smiData.RevoluteJoint(26).ID = '[Actuator Cylinder-4:-:Actuator Tube-4]';

smiData.RevoluteJoint(27).Rz.Pos = 46.083230340750589;  % deg
smiData.RevoluteJoint(27).ID = '[Actuator Cylinder-5:-:Ball Joint Revolve-5]';

smiData.RevoluteJoint(28).Rz.Pos = 166.97256173796382;  % deg
smiData.RevoluteJoint(28).ID = '[Actuator Cylinder-5:-:Actuator Tube-5]';

smiData.RevoluteJoint(29).Rz.Pos = 105.01598995301548;  % deg
smiData.RevoluteJoint(29).ID = '[Actuator Cylinder-6:-:Ball Joint Revolve-6]';

smiData.RevoluteJoint(30).Rz.Pos = -64.383955766660307;  % deg
smiData.RevoluteJoint(30).ID = '[Actuator Cylinder-6:-:Actuator Tube-6]';


%Initialize the SphericalJoint structure array by filling in null values.
smiData.SphericalJoint(12).S.Pos.Angle = 0.0;
smiData.SphericalJoint(12).S.Pos.Axis = [0.0 0.0 0.0];
smiData.SphericalJoint(12).ID = '';

%This joint has been chosen as a cut joint. Simscape Multibody treats cut joints as algebraic constraints to solve closed kinematic loops. The imported model does not use the state target data for this joint.
smiData.SphericalJoint(1).S.Pos.Angle = 137.6223151974086;  % deg
smiData.SphericalJoint(1).S.Pos.Axis = [0.9729922217819359 -0.13548902937972176 -0.18689264102578509];
smiData.SphericalJoint(1).ID = '[Ball Joint Revolve-1:-:Ball Socket 3D-2]';

%This joint has been chosen as a cut joint. Simscape Multibody treats cut joints as algebraic constraints to solve closed kinematic loops. The imported model does not use the state target data for this joint.
smiData.SphericalJoint(2).S.Pos.Angle = 116.78470301485621;  % deg
smiData.SphericalJoint(2).S.Pos.Axis = [-0.9641874824278579 0.072028782707550254 0.25525350769597205];
smiData.SphericalJoint(2).ID = '[Ball Joint Revolve-2:-:Ball Socket 3D-3]';

%This joint has been chosen as a cut joint. Simscape Multibody treats cut joints as algebraic constraints to solve closed kinematic loops. The imported model does not use the state target data for this joint.
smiData.SphericalJoint(3).S.Pos.Angle = 145.87656001928124;  % deg
smiData.SphericalJoint(3).S.Pos.Axis = [-0.98772202474843562 -0.1504722563964648 0.041991688246690861];
smiData.SphericalJoint(3).ID = '[Ball Joint Revolve-3:-:Ball Socket 3D-4]';

%This joint has been chosen as a cut joint. Simscape Multibody treats cut joints as algebraic constraints to solve closed kinematic loops. The imported model does not use the state target data for this joint.
smiData.SphericalJoint(4).S.Pos.Angle = 108.21744115959619;  % deg
smiData.SphericalJoint(4).S.Pos.Axis = [-0.981515278619566 0.12604703889991611 0.14401354735204167];
smiData.SphericalJoint(4).ID = '[Ball Joint Revolve-4:-:Ball Socket 3D-5]';

%This joint has been chosen as a cut joint. Simscape Multibody treats cut joints as algebraic constraints to solve closed kinematic loops. The imported model does not use the state target data for this joint.
smiData.SphericalJoint(5).S.Pos.Angle = 162.48186184250784;  % deg
smiData.SphericalJoint(5).S.Pos.Axis = [-0.94763725819577038 0.055679599303778714 0.31445732476851845];
smiData.SphericalJoint(5).ID = '[Ball Joint Revolve-5:-:Ball Socket 3D-6]';

%This joint has been chosen as a cut joint. Simscape Multibody treats cut joints as algebraic constraints to solve closed kinematic loops. The imported model does not use the state target data for this joint.
smiData.SphericalJoint(6).S.Pos.Angle = 32.026642557087555;  % deg
smiData.SphericalJoint(6).S.Pos.Axis = [-0.90677092654726166 -0.34996013637959805 0.2351475913416797];
smiData.SphericalJoint(6).ID = '[Ball Joint Revolve-6:-:Ball Socket 3D-1]';

smiData.SphericalJoint(7).S.Pos.Angle = 118.66631187567219;  % deg
smiData.SphericalJoint(7).S.Pos.Axis = [-0.96818726457434856 0.11038884545166694 -0.22456117989512037];
smiData.SphericalJoint(7).ID = '[Ball Joint Revolve-7:-:Ball Socket 3D-12]';

smiData.SphericalJoint(8).S.Pos.Angle = 101.16640923681418;  % deg
smiData.SphericalJoint(8).S.Pos.Axis = [-0.98860191763249006 -0.14285207881584239 -0.047534535143993667];
smiData.SphericalJoint(8).ID = '[Ball Joint Revolve-8:-:Ball Socket 3D-10]';

smiData.SphericalJoint(9).S.Pos.Angle = 149.05569006125788;  % deg
smiData.SphericalJoint(9).S.Pos.Axis = [-0.97214713265773234 -0.12655807839973859 -0.19726379611351616];
smiData.SphericalJoint(9).ID = '[Ball Joint Revolve-9:-:Ball Socket 3D-11]';

smiData.SphericalJoint(10).S.Pos.Angle = 75.927794784704318;  % deg
smiData.SphericalJoint(10).S.Pos.Axis = [-0.97008279877883208 -0.14082250755216519 0.19775840027707667];
smiData.SphericalJoint(10).ID = '[Ball Joint Revolve-10:-:Ball Socket 3D-9]';

smiData.SphericalJoint(11).S.Pos.Angle = 99.160999943119933;  % deg
smiData.SphericalJoint(11).S.Pos.Axis = [-0.97904277915103821 -0.12035442700335246 -0.16428648298903381];
smiData.SphericalJoint(11).ID = '[Ball Joint Revolve-11:-:Ball Socket 3D-8]';

smiData.SphericalJoint(12).S.Pos.Angle = 102.22008678517264;  % deg
smiData.SphericalJoint(12).S.Pos.Axis = [-0.91409384969036622 -0.26399554152355198 -0.30779666667125016];
smiData.SphericalJoint(12).ID = '[Ball Joint Revolve-12:-:Ball Socket 3D-7]';

